#!/usr/bin/python3

import database
import os
import sys

os.system('clear')
print('\n')


def front():
    print(""" 


    === === === === === === === [ CONTACTS ] === === === === === === ===

    (a)ll   (r)ecords   (c)reate   (d)elete   (f)ind   (m)odify   (q)uit



            """)
    choice = input("Selection: ")
    os.system("clear")
    return choice


def create_rec():
    name = input("Name: ")
    street = input("Street: ")
    city = input("City: ")
    state = input("State: ")
    postal = input("Zip: ")
    cell = input("Cell: ")
    email = input("Email: ")
    web = input("Web: ")
    database.create_record(name,street,city,state,postal,cell,email,web)
    return select(front())

def show_all():
    database.show_all()
    return select(front())

def show_all_records():
    database.show_all_records()
    return select(front())    

def del_rec():
    print(f"\n Delete a Record")
    print("=== === === === ===")
    
    while True:        
        try:
            id_num = int(input(f" Record number: "))
            break
        except ValueError:
            print("\n Must enter an integer!!! \n\n")

    database.del_record(str(id_num))
    return select(front())


def query_rec():
    q = input(f"\n Contact name: ")
    name = q.strip() + "%"
    os.system("clear")
    print()
    database.query_record(name)
    return select(front())


def mod_rec():
    name = input(f"\n Contact to modify: ")
    os.system("clear")
    print()
    database.modify_record(name)
    return select(front())    

def app_exit():
    return sys.exit(0)

def select(choice):
    func_map = {
        'a': show_all,
        'c': create_rec,
        'r': show_all_records,
        'd': del_rec,
        'f': query_rec,
        'm': mod_rec,
        'q': app_exit
        }

    # check if key in dict
    try:
        func_map.get(choice) is not None
        return func_map[choice]()
    except KeyError:
        print("\n ERROR!  Selection does not exist. \n\n")
        return select(front())


########################################################################

select(front())

