#!/usr/bin/python3

import sqlite3

def create_record(name,street,city,state,postal,cell,email,web):
    conn = sqlite3.connect('contact.db')
    c = conn.cursor()
    c.execute("INSERT INTO contacts VALUES (?,?,?,?,?,?,?,?) ", (name,street,city,state,postal,cell,email,web))
    conn.commit()
    conn.close()
    return

def del_record(id):
    conn = sqlite3.connect('contact.db')
    c = conn.cursor()
    c.execute("DELETE from contacts WHERE rowid = (?) ", id)
    conn.commit()
    conn.close()
    return


def query_record(name):
    conn = sqlite3.connect('contact.db')
    c = conn.cursor()
    c.execute("SELECT * FROM contacts WHERE name LIKE (?) ", [name])
    i = c.fetchone()

    print(f" {i[0]} \n {i[1]} \n {i[2]}    {i[3]}  {i[4]}\n cell: {i[5]}\n email: {i[6]}\n web: {i[7]}\n")
    conn.commit()
    conn.close()
    return


def modify_record(name):
    conn = sqlite3.connect('contact.db')
    c = conn.cursor()
    c.execute("SELECT rowid, * FROM contacts WHERE name LIKE (?) ", [name])
    i = c.fetchone()
    
    print(f" Record: {i[0]}\n (n)ame: {i[1]} \n (s)treet: {i[2]}")
    print(f" (c)ity: {i[3]}    s(t)ate: {i[4]}  c(o)de: {i[5]}")
    print(f" (p)hone: {i[6]}\n (e)mail: {i[7]}\n (w)eb: {i[8]}\n\n\n")

    rec_num = input("Record#: ")
    sel = input("Change (n, s, c, ...)? ")
    inp = input("Change to: ")

    mod_dict = {'n':'name','s':'street','c':'city','t':'state','o':'postal','p':'cell','e':'email','w':'web'}
    update_query = f"""UPDATE contacts SET {mod_dict[sel]}='{inp}' WHERE rowid={rec_num}"""
    c.execute(update_query)
    conn.commit()
    conn.close()
    return



def show_all():
    conn = sqlite3.connect('contact.db')
    c = conn.cursor()
    c.execute("SELECT * FROM contacts")
    items = c.fetchall()

    for i in items:
        print(f"\n {i[0]} \n {i[1]} \n {i[2]}   {i[3]}  {i[4]} \n  cell: {i[5]}\n email: {i[6]}\n   web: {i[7]}\n\n")

    conn.commit()
    conn.close()
    return


def show_all_records():
    conn = sqlite3.connect('contact.db')
    c = conn.cursor()
    c.execute("SELECT rowid, * FROM contacts")
    items = c.fetchall()

    for i in items:
        print(i, '\n')
       
    conn.close()
    return
